# Fidgety Remora

The intent of this project is to act as a container for ini file parsing tests for various Linux C libraries.

This should only contain a test .ini file, and the documentation for the libraries used, tests performed, their resulting output, and the recommendation.