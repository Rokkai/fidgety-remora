#!/bin/bash
# Simple log for for example inih output

make clean
make basic
./basic_parser INI_TEST_FILE.ini &> iniparser_sample_output.txt
echo " " >> iniparser_sample_output.txt
echo EOF >> iniparser_sample_output.txt
echo " " >> iniparser_sample_output.txt
./basic_parser INI_TEST_FILE-2.ini >> iniparser_sample_output.txt
