#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "iniparser.h"

int main(int argc, char * argv[])
{
    if (argc<2) 
    {
        printf("Missing argument for config file\n");
    }
    else
    {
	dictionary * ini ;
    	char * ini_name ;

        ini_name = argv[1] ;

	ini = iniparser_load(ini_name);
    	iniparser_dump(ini, stdout);
    	iniparser_freedict(ini);
    }

    return 0 ;
}
