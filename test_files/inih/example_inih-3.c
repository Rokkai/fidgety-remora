/* Example: parse a simple configuration file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ini.h>
#include <stdbool.h>


/* 
 * Struct to hold the configuration information we're specifically looking for in the .ini file. 
 */
typedef struct	   
{
    const char* unsectioned_key;
    const char* octothorp_key;
    const char* octothorp_key_2;
    const char* octothorp_key_3;
    const char* semicolon_key;
    const char* semicolon_key_2;
    const char* semicolon_key_3;
    const char* mixed_comment_key;
    const char* visible_key;
    const char* visible_key2;
    const char* key_in_commented_section;
    const char* key_in_commented_section_2;
    const char* key1;
    const char* key2;
    const char* key_with_no_value;
    const char* key_with_empty_string;
    const char* key_with_null_value;
} configuration;


/* 
 * Handler for parsing the information found in the ini file. Associates key/values with variables
 * from configuration struct.  The Match macro takes in the section and keys from the ini file 
 * and associates those keys to a variable defined in the configuration struct.
 */
static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("", "unsectioned_key")) 
    {
        pconfig->unsectioned_key = strdup(value);
    }
    /* else */
    /*if (MATCH("comments", "octothorp_key")) 
    {
        pconfig->octothorp_key = strdup(value);
    }
    else if (MATCH("comments", "octothorp_key_2")) 
    {
        pconfig->octothorp_key_2 = strdup(value);
    }
    else if (MATCH("comments", "octothorp_key_3")) 
    {
        pconfig->octothorp_key_3 = strdup(value);
    }
    else if (MATCH("comments", "semicolon_key")) 
    {
        pconfig->semicolon_key = strdup(value);
    }
    else if (MATCH("comments", "semicolon_key_2")) 
    {
        pconfig->semicolon_key_2 = strdup(value);
    }
    else if (MATCH("comments", "semicolon_key3")) 
    {
        pconfig->semicolon_key_3 = strdup(value);
    }
    else if (MATCH("comments", "mixed_comment_key")) 
    {
        pconfig->mixed_comment_key = strdup(value);
    }
    else */
    else if (MATCH("comments", "visible_key")) 
    {
        pconfig->visible_key = strdup(value);
    }
    else if (MATCH("comments", "visible_key2")) 
    {
        pconfig->visible_key2 = strdup(value);
    }
    else if (MATCH("commented_comment_section", "key_in_commented_section")) 
    {
        pconfig->key_in_commented_section = strdup(value);
    }
    else if (MATCH("commented_comment_section_2", "key_in_commented_section_2")) 
    {
        pconfig->key_in_commented_section_2 = strdup(value);
    }
    else if (MATCH("duplicates", "key1")) 
    {
        pconfig->key1 = strdup(value);
    }
    else if (MATCH("duplicates", "key1")) 
    {
        pconfig->key1 = strdup(value);
    }
    else if (MATCH("duplicates", "key2")) 
    {
        pconfig->key2 = strdup(value);
    }
    else if (MATCH("duplicates", "key2")) 
    {
        pconfig->key2 = strdup(value);
    }
    else if (MATCH("section_with_missing_stuff", "key_with_no_value")) 
    {
        pconfig->key_with_no_value = strdup(value);
    }
    else if (MATCH("section_with_missing_stuff", "key_with_empty_string")) 
    {
        pconfig->key_with_empty_string = strdup(value);
    }
    else if (MATCH("section_with_missing_stuff", "key_with_null_value")) 
    {
        pconfig->key_with_null_value = strdup(value);
    }
    else
    {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


int main(int argc, char* argv[])
{
    configuration config;

    /* Parse the file */
    if (ini_parse("TEST_file_3.ini", handler, &config) < 0) 
    {
        printf("Can't load 'TEST_file_3.ini'\n");
        return 1;
    }

    /* Print the values loaded into the configuration struct from the ini file. */
    printf("\nUnsectioned key = ",config.unsectioned_key);

    /*printf("\ncomments section:\noctothorp_key=%s,\noctothorp_key_2=%s,\noctothorp_key_3=%s,"
	   "\nsemicolon_key=%s,\nsemicolon_key_2=%s,\nsemicolon_key_3=%s,\nmixed_comment_key=%s,"
           "\nvisible_key=%s,\nvisible_key2=%s,\n\n",config.octothorp_key, config.octothorp_key_2,
           config.octothorp_key_3, config.semicolon_key, config.semicolon_key_2, config.semicolon_key_3,
           config.mixed_comment_key, config.visible_key, config.visible_key2);
*/
    printf("\ncommented_comment_sections:\noctothorp_key=%s,\nsemicolon_key=%s,\n",
           config.key_in_commented_section, config.key_in_commented_section_2);

    printf("\nduplicates sections:\nkey1=%s,\nkey2=%s,\n", config.key1, config.key2);

    printf("\nsection_with_missing_stuff:\nkey_with_no_value=%s,\nkey_with_empty_string=%s,"
           "\nkey_with_null_value=%s,\n", config.key_with_no_value, config.key_with_empty_string,
           config.key_with_null_value);

    /* free the memory used to strdup() the strings from the handler */
    free((void*)config.unsectioned_key);
    free((void*)config.octothorp_key);
    free((void*)config.octothorp_key_2);
    free((void*)config.octothorp_key_3);
    free((void*)config.semicolon_key);
    free((void*)config.semicolon_key_2);
    free((void*)config.semicolon_key_3);
    free((void*)config.mixed_comment_key);
    free((void*)config.visible_key);
    free((void*)config.visible_key2);
    free((void*)config.key_in_commented_section);
    free((void*)config.key_in_commented_section_2);
    free((void*)config.key1);
    free((void*)config.key2);
    free((void*)config.key_with_no_value);
    free((void*)config.key_with_empty_string);
    free((void*)config.key_with_null_value);

    return 0;
}
