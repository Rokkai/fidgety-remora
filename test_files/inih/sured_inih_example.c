/* Example: parse a simple configuration file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ini.h>
#include <stdbool.h>


/* 
 * Struct to hold the configuration information we're specifically looking for in the .ini file. 
 */
typedef struct	   
{
    int log_verbosity;
    int max_processes;
    const char* path;
    const char* process_stale_files;
    const char* watched_dir;
    const char* inprocess_dir;
    const char* failed_dir;
    const char* finished_dir;
} configuration;


/* 
 * Handler for parsing the information found in the ini file. Associates key/values with variables
 * from configuration struct.  The Match macro takes in the section and keys from the ini file 
 * and associates those keys to a variable defined in the configuration struct.
 */
static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("LogMode", "log_verbosity")) 
    {
        pconfig->log_verbosity = atoi(value);
    } 
    else if (MATCH("PythonEgg", "max_processes")) 
    {
        pconfig->max_processes = atoi(value);
    } 
    else if (MATCH("PythonEgg", "path")) 
    {
        pconfig->path = strdup(value);
    } 
    else if (MATCH("InotifySettings", "process_stale_files")) 
    {
        pconfig->process_stale_files = strdup(value);
    } 
    else if (MATCH("InotifySettings", "watched_dir")) 
    {
        pconfig->watched_dir = strdup(value);
    } 
    else if (MATCH("InotifySettings", "inprocess_dir")) 
    {
        pconfig->inprocess_dir = strdup(value);
    } 
    else if (MATCH("InotifySettings", "failed_dir"))  
    {
        pconfig->failed_dir = strdup(value);
    } 
    else if (MATCH("InotifySettings", "finished_dir"))  
    {
        pconfig->finished_dir = strdup(value);
    } 
    else 
    {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


int main(int argc, char* argv[])
{
    configuration config;

    /* Parse the file */
    if (ini_parse("sured.conf", handler, &config) < 0) 
    {
        printf("Can't load 'sured.conf'\n");
        return 1;
    }

    /* Print the values loaded into the configuration struct from the ini file. */
    printf("\nSettings loaded from file:\nlog_verbosity = %d,\nmax_processes = %d,\npath = %s,"
           "\nprocess_stale_files = %s,\nwatched_dir = %s,\ninprcoess_dir = %s,\nfailed_dir = %s,"
           "\nfinished_dir = %s,\n",config.log_verbosity, config.max_processes, config.path,
           config.process_stale_files, config.watched_dir, config.inprocess_dir, config.failed_dir,
           config.finished_dir);

    /* free the memory used to strdup() the strings from the handler */
    free((void*)config.path);
    free((void*)config.process_stale_files);
    free((void*)config.watched_dir);
    free((void*)config.inprocess_dir);
    free((void*)config.failed_dir);
    free((void*)config.finished_dir);

    return 0;
}
