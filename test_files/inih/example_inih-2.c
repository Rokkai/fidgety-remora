/* Example: parse a simple configuration file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ini.h>
#include <stdbool.h>


/* 
 * Struct to hold the configuration information we're specifically looking for in the .ini file. 
 */
typedef struct	   
{
    int version;
    const char* name;
    const char* email;
    const char* key1;
    float pi;
    const char* test1;
    const char* name2; /* Can only have one key-value pair. Previous "name" is overwritten unless specifically stated in struct */
    const char* foo;
    int adams;
    const char* colon_test1;
    const char* colon_test2;
    const char* colon_test3;
    const char* colon_test4;
    bool active;
} configuration;


/* 
 * Handler for parsing the information found in the ini file. Associates key/values with variables
 * from configuration struct.  The Match macro takes in the section and keys from the ini file 
 * and associates those keys to a variable defined in the configuration struct.
 */
static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("protocol", "version")) 
    {
        pconfig->version = atoi(value);
    } 
    else if (MATCH("user", "name")) 
    {
        pconfig->name = strdup(value);
    } 
    else if (MATCH("user", "email")) 
    {
        pconfig->email = strdup(value);
    } 
    else if (MATCH("user", "pi")) 
    {
        pconfig->pi = atof(value);
    } 
    else if (MATCH("user", "test1")) 
    {
        pconfig->test1 = strdup(value);
    } 
    else if (MATCH("section1", "name"))  
    {
        pconfig->name2 = strdup(value);
    } 
    /*else if (MATCH("user", "active")) 
    {
        pconfig->active = atoi(value);
    } */
    else if (MATCH("colon_tests", "foo")) /* START: Colon test compatibility check */
    {
        pconfig->foo = strdup(value);
    } 
    else if (MATCH("colon_tests", "adams")) 
    {
        pconfig->adams = atoi(value);
    }  
    else if (MATCH("colon_tests", "adams")) 
    {
        pconfig->test1 = strdup(value);
    } 
    else if (MATCH("colon_tests", "funny1")) 
    {
        pconfig->colon_test1 = strdup(value);
    } 
    else if (MATCH("colon_tests", "funny2")) 
    {
        pconfig->colon_test2 = strdup(value);
    }
    else if (MATCH("colon_tests", "funny3")) 
    {
        pconfig->colon_test3 = strdup(value);
    }
    else if (MATCH("colon_tests", "funny4")) 
    {
        pconfig->colon_test4 = strdup(value);
    }
    else 
    {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


int main(int argc, char* argv[])
{
    configuration config;

    /* Parse the file */
    if (ini_parse("INI_TEST_FILE.ini", handler, &config) < 0) 
    {
        printf("Can't load 'INI_TEST_FILE.ini'\n");
        return 1;
    }

    /* Print the values loaded into the configuration struct from the ini file. */
    printf("\nConfig loaded from 'INI_TEST_FILE.ini':\nFrom [protocol] section: version=%d,"
           "\nFrom [user] section: name=%s, email=%s\n",/* bool(active)=%s\n",*/
           config.version, config.name, config.email/*, config.active*/);

    printf("\nTest inputs from multiple sections:\nname(from different section, but same key)=%s,"
           "\npi=%f\n",config.name2, config.pi);

    printf("\nColon test inputs:\nfoo=%s,\nadams=%d,\nfunny1=%s,\nfunny2=%s,\nfunny3=%s,"
           "\nfunny4=%s\n",config.foo, config.adams, config.colon_test1, config.colon_test2,
           config.colon_test3, config.colon_test4);

    /* free the memory used to strdup() the strings from the handler */
    free((void*)config.name);
    free((void*)config.email);
    free((void*)config.name2);
    free((void*)config.foo);
    free((void*)config.colon_test1);
    free((void*)config.colon_test2);
    free((void*)config.colon_test3);
    free((void*)config.colon_test4);

    return 0;
}
